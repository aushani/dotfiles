#!/bin/sh
# $1 = package name

filelist="$1-filelist"
tags="$1-tags"

ctags --sort=foldcase -R --c++-kinds=+p --fields=+ialS --extra=+q -I./ctag-ignore -f $tags -L $filelist > /dev/null 2>&1

#CUR=0
#FILES=`cat $filelist`
#NUM=`cat $filelist | wc -l`
#for f in $FILES; do
#    ctags -a -R --sort=foldcase --c++-kinds=+p --fields=+iaS --extra=+q -I./ctag-ignore -f $tags $f > /dev/null 2>&1
#
#    PROG=$(($CUR*100/$NUM))
#    printf 'Progress: %d%%\r' "$PROG"
#    CUR=$((CUR+1))
#done

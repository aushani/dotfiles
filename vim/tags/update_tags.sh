# First make sure apt-file is install and then do:
#sudo apt-file update

# set up tags for libc, the standard C library
echo -n "Building ctags for libc6..."
apt-file list libc6-dev | grep -E -o '/usr/include/.*\.(h|hpp)' > libc6-filelist
./build_tags.sh libc6
echo " done"

# create tags for stdlibc++ and STL
echo -n "Building ctags for stdlibcpp..."
apt-file list libstdc++ | grep -E -o '/usr/include/.*\.(h|hpp)' > stdlibcpp-filelist
./build_tags.sh stdlibcpp
echo " done"

# for Boost
echo -n "Building ctags for boost..."
apt-file list libboost | grep -E -o '/usr/include/.*\.(h|hpp)' | grep -v '/usr/include/boost/typeof/' > boost-filelist
./build_tags.sh boost
echo " done"

# for gsl
echo -n "Building ctags for gsl..."
apt-file list libgsl | grep -E -o '/usr/include/.*\.(h|hpp)' > gsl-filelist
./build_tags.sh gsl
echo " done"

# for /usr/local/include
echo -n "Building ctags for local..."
find /usr/local/include > local-filelist
./build_tags.sh local
echo " done"

# for perls include
echo -n "Building ctags for perls..."
find ~/perls/build/include > perls-filelist
./build_tags.sh perls
echo " done"


# old/backup
#boost
#sudo apt-file update > /dev/null 2>&1
#apt-file list boost | grep -E -o '/usr/include/.*\.(h|hpp)' | grep -v '/usr/include/boost/typeof/' > boost_files.txt
#ctags --sort=foldcase --c++-kinds=+p --fields=+iaS --extra=+q -f ~/.vim/tags/boosttags -L boost_files.txt > /dev/null 2>&1
#
#perls
#while read dir ; do
#  eval ctags --sort=foldcase -f perlstags -a -R --c++-kinds=+p --fields=+iaS --extra=+q $dir > /dev/null 2>&1
#done < dirs.txt
#
#global
#FILES=`find /usr/include/ -mindepth 1 -maxdepth 1 -not -name boost`
#NUM=`find /usr/include/ -mindepth 1 -maxdepth 1 -not -name boost | wc -l`
#CUR=0
#for f in $FILES; do
#    ctags -a -R --sort=foldcase --c++-kinds=+p --fields=+iaS --extra=+q -f globaltags $f > /dev/null 2>&1
#
#    PROG=$(($CUR*100/$NUM))
#    printf 'Progress: %d%%\r' "$PROG"
#    CUR=$((CUR+1))
#done

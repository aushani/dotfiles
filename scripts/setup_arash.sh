#/bin/bash

sudo apt-get install ruby

sudo apt-get install i3
sudo apt-get install i3status
sudo apt-get install i3blocks
sudo apt-get install scrot

bash install_dotfiles.sh
bash install_i3_fonts.sh
bash install_fonts.sh
bash install_vimplugins.sh
bash setup_background.sh
bash setup_gnome_terminal.sh
bash setup_i3blocks.sh

# setup python i3
sudo apt-get install python-pip
sudo pip install i3-py

# install vim
sudo apt-get install vim

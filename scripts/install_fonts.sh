#!/bin/bash

DOT_DIR=$HOME/dotfiles      # dotfiles directory

FONTS_INSTALL_DIR=/usr/local/share/fonts/arash/

sudo mkdir $FONTS_INSTALL_DIR
sudo cp $DOT_DIR/fonts/* $FONTS_INSTALL_DIR

sudo fc-cache -fv

#!/bin/bash

# This script creates symlinks from the home directory to any desired dotfiles in ~/dotfiles

# list of files/folders to symlink in homedir
files="bashrc bash_aliases vimrc vim ssh gitconfig gitignore xchat2 latexmkrc git_template fonts diff.py i3"

DOT_DIR=$HOME/dotfiles      # dotfiles directory
OLDDOT_DIR=$DOT_DIR/old     # old dotfiles backup directory
START_DIR=`pwd`

# check if $OLDDOT_DIR already exists
if [ ! -d "$OLDDOT_DIR" ]; then
    echo "Backup directory doesn't exist ($OLDDOT_DIR), have you installed the dotfiles yet?"
    exit 1
fi

echo "Uninstalling dotfiles"

# move any existing dotfiles in homedir to dotfiles_old directory, then create symlinks
for file in $files; do
    echo -n "$OLDDOT_DIR/$file..."
    rm -rf $HOME/.$file
    if [ -f "$OLDDOT_DIR/.$file" ]; then
      mv $OLDDOT_DIR/.$file ~/.$file
      echo "RESTORED"
    elif [ -d "$OLDDOT_DIR/.$file" ]; then
      mv $OLDDOT_DIR/.$file ~/.$file
      echo "RESTORED"
    else
      echo "does not exist in backup."
    fi
done

# remove backup folder
rmdir $OLDDOT_DIR

cd $START_DIR

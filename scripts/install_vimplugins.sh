#!/bin/bash

VIM_DIR=$HOME/.vim      # vim directory
START_DIR=`pwd`

echo "Installing vim plugins"

mkdir -p $VIM_DIR
cd $VIM_DIR
ruby update_bundles

cd $START_DIR

#!/bin/bash

# list of files/folders to symlink in homedir
files="bashrc bash_aliases vimrc vim ssh gitconfig gitignore fonts diff.py i3 colormakerc dunstrc"

DOT_DIR=$HOME/dotfiles      # dotfiles directory
OLDDOT_DIR=$DOT_DIR/old     # old dotfiles backup directory
START_DIR=`pwd`

echo "Installing dotfiles"

# check if $OLDDOT_DIR already exists
if [ -d "$OLDDOT_DIR" ]; then
    echo "Backup directory already exists ($OLDDOT_DIR), please uninstall dotfiles to continue."
    exit 1
fi


# change to the dotfiles directory
echo "Changing to the $DOT_DIR directory"
cd $DOT_DIR

# backup dotfiles currently in home directory
echo "Creating $OLDDOT_DIR for backup of any existing dotfiles in $HOME"
mkdir -p $OLDDOT_DIR

# move any existing dotfiles in homedir to dotfiles_old directory, then create symlinks
for file in $files; do
    echo -n "$HOME/.$file..."

    if [ -f "$HOME/.$file" ]; then
      mv $HOME/.$file $OLDDOT_DIR/.$file
      echo "BACKED-UP"
    elif [ -d "$HOME/.$file" ]; then
      mv $HOME/.$file $OLDDOT_DIR/.$file
      echo "BACKED-UP"
    else
      echo "does not exist in HOME."
    fi

    ln -sf $DOT_DIR/$file $HOME/.$file
done

cd $START_DIR

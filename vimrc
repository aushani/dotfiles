

" Preamble ---------------------------------------------------------------- {{{

call pathogen#infect() "set vim path -- enables .vim/bundle directory convenience
call pathogen#helptags()
syntax on
filetype plugin indent on
set nocompatible

" }}}



" View settings ----------------------------------------------------------- {{{

set t_Co=256
set bg=dark
"set bg=light
"set cursorline
augroup CursorLine
    au!
    au VimEnter * setlocal cursorline
    au WinEnter * setlocal cursorline
    au BufWinEnter * setlocal cursorline
    au WinLeave * setlocal nocursorline
augroup END
let g:gruvbox_contrast_dark='medium'
let g:gruvbox_contrast_light='hard'

colorscheme gruvbox
"colorscheme ir_black
"colorscheme wombat256mod
"colorscheme xoria256
"colorscheme molokai_mac
"colorscheme peaksea
"colorscheme ironman

set fillchars+=vert:│

"pop-up (suggestions) menu
highlight Pmenu      ctermfg=white ctermbg=darkgrey guifg=#ff0000 guibg=#0000ff
highlight PmenuSel   ctermfg=lightgrey ctermbg=darkblue guifg=#ff0000 guibg=#0000ff

"
"highlight Pmenu      ctermfg=8 ctermbg=17 guifg=#ff0000 guibg=#0000ff
"highlight PmenuSel   ctermfg=8 ctermbg=3 guifg=#ff0000 guibg=#0000ff
"highlight PmenuSbar  ctermfg=2 ctermbg=3 guifg=#ff0000 guibg=#00ff00
"highlight PmenuThumb ctermfg=2 ctermbg=3 guifg=#ff0000 guibg=#00ff00

" }}}



" Statusline/airline------------------------------------------------------- {{{

set statusline=%F%m%r%h%w\
set statusline+=%{fugitive#statusline()}\
set statusline+=[%{strlen(&fenc)?&fenc:&enc}]
set statusline+=\ [line\ %l\/%L]

" tell VIM to always put a status line in, even if there is only one window
set laststatus=2

let g:airline#extensions#tabline#enabled = 1
let g:airline_powerline_fonts = 1
"let g:airline_theme='powerlineish'
let g:airline_theme='gruvbox'

"disable showing open buffers in statusline (only want in command line)
let g:airline#extensions#bufferline#enabled = 0
let g:bufferline_echo = 1

" }}}



" Simple shortcuts -------------------------------------------------------- {{{

"save keystrokes!
noremap ; :
noremap , ;

"avoid escape!
:imap jk <Esc>
:imap Jk <Esc>
:imap jK <Esc>
:imap JK <Esc>

"quick add char before/after
nmap <space> i <esc>r
nmap <tab> a <esc>r

"tab next/previous
map <S-h> gT
map <S-l> gt

"buffer next/previous
map <C-h> :bprevious<CR>
map <C-l> :bnext<CR>

"up/down at constant line offset
noremap <C-J> gj
noremap <C-K> gk

"quick edit/source vimrc
nmap ,v :tabnew ~/.vimrc<return>
nmap ,s :source ~/.vimrc<return>

nmap ,d a/*{{{*/<ESC>
nmap ,f a/*}}}*/<ESC>
nmap ,D o/*{{{*/<ESC>
nmap ,F o/*}}}*/<ESC>
nmap ,c I//<ESC>

vnoremap > >gv
vnoremap < <gv

noremap <Up> <NOP>
noremap <Down> <NOP>
noremap <Left> <NOP>
noremap <Right> <NOP>

" }}}



" Basic options ----------------------------------------------------------- {{{

set encoding=utf-8
set hidden " hide buffers

"keep a longer history
set history=1000

"time out on key codes but not mappings
set notimeout
set ttimeout
set ttimeoutlen=10

"set nowrap
set number
set ruler
set scrolloff=4
set laststatus=2
set autochdir

"editing
set bs=2
set foldmethod=marker

"no default folding
set foldlevelstart=99


"backups
set backup
set backupdir=~/.vim/backup
set backupext=~

"for swap files
"set directory=~/.vim/swap,. "this got super annoying
set noswapfile

"indent
set autoindent
set smartindent

"searching
set smartcase
set ignorecase
set hlsearch
set incsearch
set showmatch
"undo search highlighting with \-space
nnoremap <silent><leader><space> :noh<cr>
"set nohlsearch "don't highlight search
"set noincsearch "don't search as you type

set splitbelow "default split direction
set splitright "default split direction

"tabbing
set splitbelow
set expandtab
set tabstop=2
set shiftwidth=2
set textwidth=80
set wrap

"shutup
"set visualbell
set novisualbell
set noerrorbells

" mouse
set mouse=r
set ttymouse=xterm2

"completion/wildcard menu
set wildmode=longest,list,full
set wildmenu
set wildignore=*.swp,*.bak,*.d
set wildignore+=*/.svn/*,/*.hg/*,/*.git/* " Version control
set wildignore+=*/.virtualenvs/*
set wildignore+=*.aux,*.out,*.toc " LaTeX stuff
set wildignore+=*.jpg,*.bmp,*.gif,*.png,*.jpeg "Pics
set wildignore+=*.o,*.obj,*.pyc,*.class "compiled files, bytecode
set wildignore+=*.DS_Store
set wildignore+=*.pdf,*.xls,*.xlsx,*.doc
set wildignore+=*.jar

"set completeopt=menuone,menu,longest,preview
set completeopt=menu,preview
"autoclose preview window
if has("autocmd")
    "auto close preview on cursor move
    "autocmd CursorMovedI * if pumvisible() == 0|pclose|endif
    "auto close preview on leave insert
    autocmd InsertLeave * if pumvisible() == 0|pclose|endif
endif

" sudo to write
cnoremap w!! w !sudo tee % >/dev/nullndif

" no double space after periods!
set nojoinspaces

" }}}


" Vim-clang-format -------------------------------------------------------- {{{

nnoremap <C-f> :ClangFormat<CR>
vnoremap <C-f> :ClangFormat<CR>
let g:clang_format#command='clang-format-3.6'
let g:clang_format#auto_format=0

let g:clang_format#style_options = {
      \ "Standard" : "C++11",
            \ "ColumnLimit" : 120 }

" }}}

" ------------------------------------------------------------------------- {{{


" Autocmd ----------------------------------------------------------------- {{{

" resize window splits on terminal resize
au VimResized * :wincmd =

autocmd BufRead,BufNewFile *.py setlocal expandtab tabstop=4 shiftwidth=4 foldmethod=indent foldnestmax=3
autocmd BufRead,BufNewFile *.sql setlocal expandtab foldmethod=indent
autocmd BufRead,BufNewFile Makefile,makefile setlocal noexpandtab
autocmd BufRead,BufNewFile *.c,*.C,*.cpp,*.h,*.cc setlocal cindent
autocmd BufRead,BufNewFile *.cu.cc,*.cu.h set filetype=cuda
autocmd BufRead,BufNewFile *.java setlocal cindent
autocmd BufRead,BufNewFile *.tex set spell textwidth=80

" }}}



" Make -------------------------------------------------------------------- {{{

set makeprg="make"
nmap ,m :MAKE <return>
nmap ,n :cnext <return>

" }}}



" Abbreviations ----------------------------------------------------------- {{{

"iab sop System.out.print
"iab sol System.out.println
"iab sep System.err.print
"iab sel System.err.println
"iab pritn print
" working on it... iab tc:\([ a-zA-Z_.0-9()\[\]\"\'\r\n]\+\):\([a-zA-Z]\+\):\([a-zA-Z]\+\):\([ a-zA-Z_.0-9()\[\]\"\';]\+\) try\ {\ \1\ }\ catch(\2\ \3)\ {\ \4\ }
iab #i #include
iab #d #define
iab teh the
iab tihs this
ca maek make
ca amek make
ca amke make
ca amk mak
ca mka mak


" }}}



" ctags ------------------------------------------------------------------- {{{

set tags=ctags_file;/
set tags+=~/ngv/ctags_file
set tags+=~/dngv/ctags_file
set tags+=~/.vim/tags/libc6-tags
"set tags+=~/.vim/tags/stdlibcpp-tags
"set tags+=~/.vim/tags/boost-tags "commented out because it's huge for YCM
set tags+=~/.vim/tags/gsl-tags
"set tags+=~/.vim/tags/local-tags "commented out because it's huge for YCM
set tags+=~/.vim/tags/perl-tags

" Ctrl + ] = go to definition, over current buffer
" Ctrl + t = return from definition
" Ctrl + w, Ctrl + ] = open definition in horizontal split
" Ctrl + \ = open definition in new tab
map <C-\> :tab split<CR>:exec("tag ".expand("<cword>"))<CR>
map <C-W><C-[> :vsp <CR>:exec("tag ".expand("<cword>"))<CR>
map <C-p> <C-W><C-}>
map <Leader>[ :tp <CR>
map <Leader>] :tn <CR>

" }}}



" OmniCppComplete --------------------------------------------------------- {{{

"let OmniCpp_NamespaceSearch = 1
"let OmniCpp_GlobalScopeSearch = 1
"let OmniCpp_ShowAccess = 1
"let OmniCpp_MayCompleteDot = 1
"let OmniCpp_MayCompleteArrow = 1
"let OmniCpp_MayCompleteScope = 1
"let OmniCpp_DefaultNamespaces = ["std", "_GLIBCXX_STD"]

" automatically open and close the popup menu / preview window
"au CursorMovedI,InsertLeave * if pumvisible() == 0|silent! pclose|endif

" }}}



" SuperTab ---------------------------------------------------------------- {{{

let g:SuperTabDefaultCompletionType = "<c-n>"

" }}}



" Syntastic --------------------------------------------------------------- {{{

let g:syntastic_enable_signs = 1
let g:syntastic_auto_loc_list=2
"let g:syntastic_python_checker = 'flake8'
let g:syntastic_stl_format = '[%E{%e Errors}%B{, }%W{%w Warnings}]'
let g:syntastic_cpp_compiler = 'clang++'
let g:syntastic_cpp_compiler_options = ' -std=c++11 -stdlib=libc++ -Wall'
"erlang
let g:syntastic_erlang_checkers=['syntaxerl']

" }}}



" YouCompleteMe ----------------------------------------------------------- {{{

let g:ycm_collect_identifiers_from_tags_files = 1
let g:ycm_filetype_specific_completion_to_disable = {'erl':1, 'hrl':1}
let g:ycm_confirm_extra_conf = 0
let g:ycm_add_preview_to_completeopt = 1
let g:ycm_seed_identifiers_with_syntax = 0
let g:ycm_key_invoke_completion = '<C-q>'
let g:ycm_show_diagnostics_ui = 0
let g:ycm_enable_diagnostic_signs = 0
let g:ycm_enable_diagnostic_highlighting = 0
nmap <silent><Leader> cm :YcmForceCompileAndDiagnostics<CR>

" }}}



" Gist (for copying buffer/visual to github gists) ------------------------ {{{

"note - set github user
let g:gist_clip_command = 'xclip -selection clipboard'
let g:gist_detect_filetype = 1
let g:gist_open_browser_after_post = 1
let g:gist_browser_command = 'google-chrome %URL%'
let g:gist_post_private = 1

" }}}



" Fugitive ---------------------------------------------------------------- {{{

nnoremap <leader>gd :Gdiff<cr>
nnoremap <leader>gs :Gstatus<cr>
nnoremap <leader>gw :Gwrite<cr>
nnoremap <leader>ga :Gadd<cr>
nnoremap <leader>gb :Gblame<cr>
nnoremap <leader>gco :Gcheckout<cr>
nnoremap <leader>gci :Gcommit<cr>
nnoremap <leader>gm :Gmove<cr>
nnoremap <leader>gr :Gremove<cr>
nnoremap <leader>gl :Shell git gl -18<cr>:wincmd \|<cr>

" }}}



" ------------------------------------------------------------------------- {{{


" }}}

"language specific
let java_highlight_java_lang_ids=1
let java_highlight_java_io=1
let java_highlight_debug=1


" If you are using a console version of Vim, or dealing
" with a file that changes externally (e.g. a web server log)
" then Vim does not always check to see if the file has been changed.
" The GUI version of Vim will check more often (for example on Focus change),
" and prompt you if you want to reload the file.
"
" There can be cases where you can be working away, and Vim does not
" realize the file has changed. This command will force Vim to check
" more often.
"
" Calling this command sets up autocommands that check to see if the
" current buffer has been modified outside of vim (using checktime)
" and, if it has, reload it for you.
"
" This check is done whenever any of the following events are triggered:
" * BufEnter
" * CursorMoved
" * CursorMovedI
" * CursorHold
" * CursorHoldI
"
" In other words, this check occurs whenever you enter a buffer, move the cursor,
" or just wait without doing anything for 'updatetime' milliseconds.
"
" Normally it will ask you if you want to load the file, even if you haven't made
" any changes in vim. This can get annoying, however, if you frequently need to reload
" the file, so if you would rather have it to reload the buffer *without*
" prompting you, add a bang (!) after the command (WatchForChanges!).
" This will set the autoread option for that buffer in addition to setting up the
" autocommands.
"
" If you want to turn *off* watching for the buffer, just call the command again while
" in the same buffer. Each time you call the command it will toggle between on and off.
"
" WatchForChanges sets autocommands that are triggered while in *any* buffer.
" If you want vim to only check for changes to that buffer while editing the buffer
" that is being watched, use WatchForChangesWhileInThisBuffer instead.
"
command! -bang WatchForChanges                  :call WatchForChanges(@%,  {'toggle': 1, 'autoread': <bang>0})
command! -bang WatchForChangesWhileInThisBuffer :call WatchForChanges(@%,  {'toggle': 1, 'autoread': <bang>0, 'while_in_this_buffer_only': 1})
command! -bang WatchForChangesAllFile           :call WatchForChanges('*', {'toggle': 1, 'autoread': <bang>0})
" WatchForChanges function
"
" This is used by the WatchForChanges* commands, but it can also be
" useful to call this from scripts. For example, if your script executes a
" long-running process, you can have your script run that long-running process
" in the background so that you can continue editing other files, redirects its
" output to a file, and open the file in another buffer that keeps reloading itself
" as more output from the long-running command becomes available.
"
" Arguments:
" * bufname: The name of the buffer/file to watch for changes.
"     Use '*' to watch all files.
" * options (optional): A Dict object with any of the following keys:
"   * autoread: If set to 1, causes autoread option to be turned on for the buffer in
"     addition to setting up the autocommands.
"   * toggle: If set to 1, causes this behavior to toggle between on and off.
"     Mostly useful for mappings and commands. In scripts, you probably want to
"     explicitly enable or disable it.
"   * disable: If set to 1, turns off this behavior (removes the autocommand group).
"   * while_in_this_buffer_only: If set to 0 (default), the events will be triggered no matter which
"     buffer you are editing. (Only the specified buffer will be checked for changes,
"     though, still.) If set to 1, the events will only be triggered while
"     editing the specified buffer.
"   * more_events: If set to 1 (the default), creates autocommands for the events
"     listed above. Set to 0 to not create autocommands for CursorMoved, CursorMovedI,
"     (Presumably, having too much going on for those events could slow things down,
"     since they are triggered so frequently...)
function! WatchForChanges(bufname, ...)
  " Figure out which options are in effect
  if a:bufname == '*'
    let id = 'WatchForChanges'.'AnyBuffer'
    " If you try to do checktime *, you'll get E93: More than one match for * is given
    let bufspec = ''
  else
    if bufnr(a:bufname) == -1
      echoerr "Buffer " . a:bufname . " doesn't exist"
      return
    end
    let id = 'WatchForChanges'.bufnr(a:bufname)
    let bufspec = a:bufname
  end
  if len(a:000) == 0
    let options = {}
  else
    if type(a:1) == type({})
      let options = a:1
    else
      echoerr "Argument must be a Dict"
    end
  end
  let autoread    = has_key(options, 'autoread')    ? options['autoread']    : 0
  let toggle      = has_key(options, 'toggle')      ? options['toggle']      : 0
  let disable     = has_key(options, 'disable')     ? options['disable']     : 0
  let more_events = has_key(options, 'more_events') ? options['more_events'] : 1
  let while_in_this_buffer_only = has_key(options, 'while_in_this_buffer_only') ? options['while_in_this_buffer_only'] : 0
  if while_in_this_buffer_only
    let event_bufspec = a:bufname
  else
    let event_bufspec = '*'
  end
  let reg_saved = @"
  "let autoread_saved = &autoread
  let msg = "\n"
  " Check to see if the autocommand already exists
  redir @"
    silent! exec 'au '.id
  redir END
  let l:defined = (@" !~ 'E216: No such group or event:')
  " If not yet defined...
  if !l:defined
    if l:autoread
      let msg = msg . 'Autoread enabled - '
      if a:bufname == '*'
        set autoread
      else
        setlocal autoread
      end
    end
    silent! exec 'augroup '.id
      if a:bufname != '*'
        "exec "au BufDelete    ".a:bufname . " :silent! au! ".id . " | silent! augroup! ".id
        "exec "au BufDelete    ".a:bufname . " :echomsg 'Removing autocommands for ".id."' | au! ".id . " | augroup! ".id
        exec "au BufDelete    ".a:bufname . " execute 'au! ".id."' | execute 'augroup! ".id."'"
      end
        exec "au BufEnter     ".event_bufspec . " :checktime ".bufspec
        exec "au CursorHold   ".event_bufspec . " :checktime ".bufspec
        exec "au CursorHoldI  ".event_bufspec . " :checktime ".bufspec
      " The following events might slow things down so we provide a way to disable them...
      " vim docs warn:
      "   Careful: Don't do anything that the user does
      "   not expect or that is slow.
      if more_events
        exec "au CursorMoved  ".event_bufspec . " :checktime ".bufspec
        exec "au CursorMovedI ".event_bufspec . " :checktime ".bufspec
      end
    augroup END
    let msg = msg . 'Now watching ' . bufspec . ' for external updates...'
  end
  " If they want to disable it, or it is defined and they want to toggle it,
  if l:disable || (l:toggle && l:defined)
    if l:autoread
      let msg = msg . 'Autoread disabled - '
      if a:bufname == '*'
        set noautoread
      else
        setlocal noautoread
      end
    end
    " Using an autogroup allows us to remove it easily with the following
    " command. If we do not use an autogroup, we cannot remove this
    " single :checktime command
    " augroup! checkforupdates
    silent! exec 'au! '.id
    silent! exec 'augroup! '.id
    let msg = msg . 'No longer watching ' . bufspec . ' for external updates.'
  elseif l:defined
    let msg = msg . 'Already watching ' . bufspec . ' for external updates'
  end
  "echo msg
  let @"=reg_saved
endfunction

let autoreadargs={'autoread':0}
execute WatchForChanges("*",autoreadargs)

autocmd BufWritePre *.c :%s/\s\+$//e

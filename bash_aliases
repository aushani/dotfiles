gopen() {
    nohup xdg-open $1 > /dev/null 2>&1 &
}

alias g=gopen

alias matlab='/usr/local/MATLAB/R2016b/bin/matlab -nosplash -nodesktop'
#alias matlab='matlab -nodesktop -nosplash'
alias make='colormake'
alias ssh-gitlab='ssh -N gitlab'
alias nautilus='nautilus --no-desktop'
alias todo='vim ~/.arash_todo'

# original script
#    http://www.macosxhints.com/article.php?story=20030217172653485
#    author: Shane Celis <shane (at) gnufoo (dot) org>
#
# Sun, 20-May-2007; 06:47:22
#    minor changes...

trashit() {

    for file in "$@"; do
            # get just file name
            destfile="`basename \"$file\"`"
            suffix='';
            i=0;

            # If that file already exists, change the name
            while [ -e "$HOME/.Trash/${destfile}${suffix}" ]; do
                    suffix="-copy$i";
                    i=`expr $i + 1`
            done

            mv -i "$file" "$HOME/.Trash/${destfile}${suffix}"
    done
}

alias rm=trashit

# https://documentation.its.umich.edu/vpn/vpn-linux-vpn-instructions
# Download cisco client from http://www.umich.edu/~waltr/linux-vpn/anyconnect-linux64-4.7.04056.tar.gz
vpn() {
  echo Type in \"connect umvpn3.umnet.umich.edu\"
  echo When done, type in \"disconnect\"
  /opt/cisco/anyconnect/bin/vpn
}

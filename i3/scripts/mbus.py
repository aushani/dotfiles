import json
import sys
import urllib2

stopid = sys.argv[1]
routeid = sys.argv[2]

url = "https://mbus.doublemap.com/map/v2/eta?stop=%s&route=%s" % (stopid, routeid)
content = urllib2.urlopen(url, timeout = 10).read()

s = "No buses"

try:
    res = json.loads(content)
    times = res['etas'].get(stopid).get('etas')

    s = "%sm" % (times[0].get('avg'))
    s = "%sm %sm" % (times[0].get('avg'), times[1].get('avg'))

except:
    pass

print s



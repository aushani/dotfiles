#!/usr/bin/python3

import subprocess

cmd = "upower -i /org/freedesktop/UPower/devices/battery_BAT0"

res = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE).stdout.read()

units = None
time = None

for line in res.split(b"\n"):
    tokens = line.split(b" ")

    if len(tokens)<=4:
        continue

    if tokens[4] == b'state:':
        state = tokens[-1]

    if tokens[4] == b'percentage:':
        percentage = tokens[-1]

    if tokens[4] == b'time':
        num = tokens[-2]
        units = tokens[-1]

    if tokens[2]==b'updated:':
        count = tokens[-3]
        count = count[1:]

if units == 'hours':
    hours = int(float(num))
    mins = (float(num) - hours)*60
    time = '(%01d:%02d)' % (hours, mins)

if units == 'minutes':
    hours = 0
    mins = int(float(num))
    time = '(%01d:%02d)' % (hours, mins)

if units is None:
    time = None

if state == b'discharging':
    state = 'DIS'

if state == b'charging':
    state = 'CHR'

if state == b'fully-charged':
    state = 'FULL'

percentage = int(percentage[:-1])

# testing
#percentage = int(count)*5 % 100

rgb1 = ()
rgb2 = ()
frac = 0.0

if percentage < 40:
    rgb1 = (0xFF, 0, 0)
    rgb2 = (0xFF, 0xAE, 0)
    frac = (40 - percentage) / 40.0
elif percentage < 60:
    rgb1 = (0xFF, 0xAE, 0)
    rgb2 = (0xFF, 0xF6, 0)
    frac = (60 - percentage) / 20.0
elif percentage < 80:
    rgb1 = (0xFF, 0xF6, 0)
    rgb2 = (0xA8, 0xFF, 0)
    frac = (80 - percentage) / 20.0
elif percentage < 100:
    rgb1 = (0xA8, 0xFF, 0)
    rgb2 = (0x00, 0xFF, 0)
    frac = (100 - percentage) / 20.0
else:
    rgb1 = (0x00, 0xFF, 0)
    rgb2 = (0x00, 0xFF, 0)
    frac = 0

a = frac
b = 1.0 - frac

rgb = [0, 0, 0]
for i in range(3):
    rgb[i] = int(rgb1[i]*a + rgb2[i]*b)

color = '#%02x%02x%02x' % tuple(rgb)

if state is None and time is None:
    print('<span foreground=\'%s\'>%s%%</span>' % (color, percentage))
elif time is None:
    print('<span foreground=\'%s\'>%s%% %s</span>' % (color, percentage, state))
else:
    print('<span foreground=\'%s\'>%s%% %s %s</span>' % (color, percentage, state, time))
